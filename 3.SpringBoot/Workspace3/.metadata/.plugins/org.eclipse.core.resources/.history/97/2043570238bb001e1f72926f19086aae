package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.model.Employee;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	private JavaMailSender mailSender;
	@GetMapping
	public List<Employee> getEmployees() {
		return employeeRepository.findAll();
	}

	public Employee getEmployeeById(int employeeId) {
		return employeeRepository.findById(employeeId).orElse(null);
	}

	public Employee getEmployeeByName(String employeeName) {
		return employeeRepository.findByName(employeeName);
	}

	public Employee employeeLogin(String emailId, String password) {
		return employeeRepository.employeeLogin(emailId, password);
	}

	@PostMapping

	public Employee addEmployee(Employee employee) {

		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPwd = bcrypt.encode(employee.getPassword());
		employee.setPassword(encryptedPwd);

		// Save the employee
		Employee savedEmployee = employeeRepository.save(employee);

		// Send a welcome email
		sendWelcomeEmail(savedEmployee);

		return savedEmployee;
	}

	private void sendWelcomeEmail(Employee employee) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(employee.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + employee.getEmpName() + ",\n\n"
				+ "Thank you for registering ");

		mailSender.send(message);
	}

	public Employee updateEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public void deleteEmployeeById(int employeeId) {
		employeeRepository.deleteById(employeeId);
	}
}