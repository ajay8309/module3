package day02;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//Insert Employee Record
public class insert {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.println("Enter Employee Id, Name, Salary, Gender, EmailId, Password");
		Scanner scan = new Scanner(System.in);
		int empId = scan.nextInt();
		String empName = scan.next();
		double salary = scan.nextDouble();
		String gender = scan.next();
		String emailId = scan.next();
		String password = scan.next();
		System.out.println();

		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "SELECT * FROM employee";

		String insertQuery = "insert into employee values " + "(" + empId + ", '" + empName + "', " + salary + ", '"
				+ gender + "', '" + emailId + "', '" + password + "')";

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int result = stmt.executeUpdate(insertQuery);
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				System.out.println("EmpId : " + rs.getInt(1));
				System.out.println("EmpName : " + rs.getString("empName"));
				System.out.println("Salary : " + rs.getDouble(3));
				System.out.println("Gender : " + rs.getString("gender"));
				System.out.println("EmailId : " + rs.getString(5));
				System.out.println("Password: " + rs.getString(6) + "\n");
			}

			if (result > 0) {
				System.out.println("Employee Record Inserted");
			} else {
				System.out.println("Failed to Insert the Employee Record!!!");
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}
}
