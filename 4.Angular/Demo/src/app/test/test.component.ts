import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {

  id: number;
  name: string;
  avg: number;

  address: any;
  hobbies: any;

  constructor() {
    // alert("constructor invoked...")

    this.id = 101;
    this.name = 'Harsha';
    this.avg = 45.46;

    this.address = {
      streetNo: 101,
      city: 'Khammam',
      state: 'Telangana'
    };

    this.hobbies = ['Sleeping','Eating']
  }
  ngOnInit() {
    // alert("ngOnInit invoked...")
  }
}
