// cart.component.ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: any;
  cartProducts: any;
  

  constructor() {}

  ngOnInit() {
    this.cartProducts = localStorage.getItem("cartItems");
    this.cartItems = JSON.parse(this.cartProducts);
  }

  calculateTotal(): number {
    if (this.cartItems && this.cartItems.length > 0) {
      return this.cartItems.reduce((total: number, product: any) => total + product.price, 0);
    } else {
      return 0;
    }
  }
}
