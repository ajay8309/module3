import { Component } from '@angular/core';
import { Router } from '@angular/router';

//Import EmpService
import { EmpService } from '../emp.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
  //Dependency Injection for EmpService and Router
  constructor(private router: Router, private service: EmpService,private toastr:ToastrService) {

    localStorage.removeItem('emailId');
    localStorage.clear();

    //Setting the isUserLoggedIn variable value to false under EmpService
    this.service.setIsUserLoggedOut();

       // Show a success toaster message
    this.toastr.success('Logout sucessfull!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 5000, // 5 seconds
    });

    // Assuming you want to navigate to the login page after registration
    this.router.navigate(['login']);
  }


  ngOnInit() {
  }

}
